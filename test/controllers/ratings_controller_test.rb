require 'test_helper'

class RatingsControllerTest < ActionController::TestCase
  test "should get article" do
    get :article
    assert_response :success
  end

  test "should get comment" do
    get :comment
    assert_response :success
  end

end
