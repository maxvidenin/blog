var embedCode = {};

embedCode.bindEmbed = function(){
    var container = $(this);
    container.find('a').each(function(){
        $(this).text('#'+$(this).text()+'#');
    });
    var text = container.html();
    text = text.replace(/(\s|>|^)(https?:[^\s<]*)/igm,'$1<a href="$2" class="embed">$2</a>')
        .replace(/(\s|>|^)(mailto:[^\s<]*)/igm,'$1<a href="$2" class="embed">$2</a>')
        .replace(/\#/igm,'');
    container
        .empty()
        .html(text)
        .find('.embed')
        .oembed(null,{
            maxWidth: (container.width() * 90) / 100 // 90% of container width
        });
}

$(function(){

});
