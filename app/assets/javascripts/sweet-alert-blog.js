var sweetAlertMessage = {};

sweetAlertMessage.init = function(options) {
    this.defaults = {
            title: "Are you sure?",
            text: "You will not be able to recover this item!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false,
            removeWrapperSelector: '.delete-sweet-alert-wrapper',
            bindEventSelector: 'a.delete-sweet-alert',
            itemText: 'item'
        };
    this.settings = $.extend({}, this.defaults, options);
};

sweetAlertMessage.binDeleteEvent = function() {
    var settings = this.settings;
    $(document).on('click', settings.bindEventSelector, function(e){
        var self = this;
        e.preventDefault();
        swal(settings,
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: $(self).attr('href'),
                        method: 'DELETE',
                        success: function (r) {
                            if (r.success) {
                                $(self).parents(settings.removeWrapperSelector).remove();
                                swal("Deleted!", "Your " + settings.itemText + " has been deleted.", "success");
                            } else {
                                swal("Cancelled", "Your " + settings.itemText + " is safe :)", "error");
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr.statusCode);
                            console.log(xhr.statusText);
                            swal("Cancelled", "Your " + settings.itemText + " is safe :)", "error");
                        }
                    });
                } else {
                    swal("Cancelled", "Your " + settings.itemText + " is safe :)", "error");
                }
            });
        return false;
    });
};
