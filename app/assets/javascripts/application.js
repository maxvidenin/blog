// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
// TODO investigate problem with turbolinks gem
// require turbolinks
//= require jquery.oembed
//= require_tree .
$(function() {
    // ajax search, pagination [start]
    $(document).on("click", "#articles .digg_pagination a", function(e) {
        e.preventDefault();
        $.getScript(this.href);
        return false;
    });
    $(document).on("keyup", "#articles_search input", function(e) {
        $.get($("#articles_search").attr("action"), $("#articles_search").serialize(), null, "script");
        return false;
    });
    $("#articles_search").submit(function() {
        $.get(this.action, $(this).serialize(), null, "script");
        return false;
    });
    // ajax search, pagination [end]

    // oembed [start]
    $('.embed').each(embedCode.bindEmbed);
    // oembed [end]

    // sweetAlert [start]
    sweetAlertMessage.init();
    sweetAlertMessage.binDeleteEvent();
    // sweetAlert [end]
});
