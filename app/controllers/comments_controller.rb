class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @article_id = params[:article_id]
    @article = Article.find(@article_id)
    @comments = @article.comments.order(:created_at => :desc).paginate(:page => params[:page], :per_page => 5)
    @comment = @article.comments.create(comment_params.merge!({:user_id => current_user.id}))
    if @comment.valid?
      redirect_to article_path(@article)
    else
      render 'articles/show'
      # redirect_to article_path(@article), alert: 'Your book was not found'
    end
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    authorize! :destroy, @comment
    if @comment.destroy
      render :json => {:success => true}
    elsif
      render :json => {:success => false}
    end
    # redirect_to article_path(@article)
  end

  private
  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end
end
