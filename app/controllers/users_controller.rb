class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @articles = @user.articles.paginate(page:params[:page])
  end

  def index
    @users = User.all().paginate(page:params[:page])
  end

  def subscriptions
    @user = User.find(params[:id])
    @users = @user.subscribing_users.paginate(page:params[:page])
  end

  def subscribers
    @user = User.find(params[:id])
    @users = @user.subscribers.paginate(page:params[:page])
  end
end