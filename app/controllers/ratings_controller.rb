class RatingsController < ApplicationController
  before_action :authenticate_user!
  respond_to :html, :js

  def article
    @article = Article.find(params[:article][:id])
    @article.rate!(current_user.id, params[:article][:rating])
    respond_with @article
  end

  def comment
    @comment = Comment.find(params[:comment][:id])
    @comment.rate!(current_user.id, params[:comment][:rating])
    respond_with @comment
  end
end
