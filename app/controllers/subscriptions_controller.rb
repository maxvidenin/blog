class SubscriptionsController < ApplicationController
  before_action :authenticate_user!
  respond_to :html, :js

  def create
    @user = User.find(params[:subscription][:author_id])
    current_user.subscribe!(@user)
    respond_with @user
  end

  def destroy
    @user = Subscription.find(params[:id]).author
    current_user.unsubscribe!(@user)
    respond_with @user
  end
end
