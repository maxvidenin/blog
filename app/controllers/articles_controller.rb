class ArticlesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  respond_to :html

  def index
    if user_signed_in?
      @articles = current_user.feed.recent.search(params[:search]).paginate(:page => params[:page], :per_page => 10)
    else
      @articles = Article.all().recent.search(params[:search]).paginate(:page => params[:page], :per_page => 10)
    end
  end

  def show
    @article = Article.find(params[:id])
    # @comments = @article.comments.recent.paginate(:page => params[:page], :per_page => 5)
    @comments = @article.comments
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
    authorize! :update, @article
  end

  def create
    @article = Article.new(article_params.merge!({:user_id => current_user.id}))
    if @article.save
      flash[:notice] = "The article has created successfully."
      send_new_article_notification(@article.id)
    end
    respond_with(@article)
  end

  def update
    @article = Article.find(params[:id])
    authorize! :update, @article
    if @article.update(article_params)
      flash[:notice] = "The article has updated successfully."
    end
    respond_with(@article)
  end

  def destroy
    @article = Article.find(params[:id])
    authorize! :destroy, @article
    if @article.destroy
      render :json => {:success => true}
    elsif
      render :json => {:success => false}
    end
  end

  private
  def article_params
    params.require(:article).permit(:title, :text)
  end

  def send_new_article_notification(article_id)
    NewArticleMailWorker.perform_async(article_id)
  end
end
