class UserMailer < ActionMailer::Base
  default from: "videnin10@gmail.com"

  def new_article(user, article)
    @article = article
    @user = user
    attachments['rails.png'] = File.read("#{Rails.root}/public/images/rails.png")
    mail(:to => user.email, :subject => 'A new article has been created.')
  end

  def new_subscriber(author, subscriber)
    @subscriber = subscriber
    @author = author
    attachments['rails.png'] = File.read("#{Rails.root}/public/images/rails.png")
    mail(:to => author.email, :subject => 'A new user subscriber.')
  end
end
