class Subscription < ActiveRecord::Base
  belongs_to :author, class_name: 'User'
  belongs_to :subscriber, class_name: 'User'
  after_save :send_user_notification

  def send_user_notification
    NewSubscriberMailWorker.perform_async(author_id, subscriber_id)
  end
end
