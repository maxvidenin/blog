class User < ActiveRecord::Base
  ROLE_ADMIN = :admin
  ROLE_ARTICLE_MODERATOR = :article_moderator
  ROLE_COMMENT_MODERATOR = :comment_moderator
  ROLE_USER_MANAGER = :user_manager
  ROLE_USER = :user

  has_many :comments
  has_many :articles

  has_many :subscriptions, foreign_key: :subscriber_id, dependent: :destroy
  has_many :subscribing_users, through: :subscriptions, source: :author
  has_many :reverse_subscriptions, foreign_key: :author_id,
           class_name:  'Subscription',
           dependent:   :destroy
  has_many :subscribers, through: :reverse_subscriptions, source: :subscriber

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar, :styles => { :large => "200x200>",:medium => "100x100>", :small => "50x50>" },
                    :default_url => 'default_avatar.png',
                    :url  => "/assets/users/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/assets/users/:id/:style/:basename.:extension"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates :first_name, :presence => true, :format => { :with => /\A[a-zA-Z]+\z/, :message => "must contain only letters"}
  validates :last_name, :presence => true, :format => { :with => /\A[a-zA-Z]+\z/, :message => "must contain only letters"}

  def get_name
    first_name + ' ' + last_name
  end

  def self.get_roles
    {
        ROLE_ADMIN => ROLE_ADMIN.to_s.humanize,
        ROLE_ARTICLE_MODERATOR => ROLE_ARTICLE_MODERATOR.to_s.humanize,
        ROLE_COMMENT_MODERATOR => ROLE_COMMENT_MODERATOR.to_s.humanize,
        ROLE_USER_MANAGER => ROLE_USER_MANAGER.to_s.humanize,
        ROLE_USER => ROLE_USER.to_s.humanize
    }
  end

  def feed
    @articles = Article.from_subscribing_users_by(self)
  end

  def subscribing?(other_user)
    subscriptions.find_by(author_id: other_user.id)
  end

  def subscribe!(other_user)
    subscriptions.create!(author_id: other_user.id)
  end

  def unsubscribe!(other_user)
    subscriptions.find_by(author_id: other_user.id).destroy!
  end
end
