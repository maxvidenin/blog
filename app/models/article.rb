class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :article_ratings, dependent: :destroy
  validates :title, presence: true
  validates :text, presence: true, length: {maximum: 5000}
  validate :check_cruel_content
  scope :recent, lambda { order('created_at DESC') }

  @@cruel_words = [
      :bad,
      :sex,
      :boobs,
      :dick,
      :fuck
  ]

  def check_cruel_content
    @words = text.split(' ')
    @words.each do |word|
      if @@cruel_words.include?(word.to_sym)
        errors.add(:text, "can't contain cruel content")
        break
      end
    end
  end

  def user_rating(user)
    user_rating = article_ratings.find_by(user_id: user.id)
    user_rating ? user_rating.rating : 0
  end

  def rate!(user_id, rate)
    article_rating = ArticleRating.find_by(user_id: user_id, article_id: id)
    if article_rating
      if article_rating.rating != 0
        new_rating = (rating - article_rating.rating) + rate.to_i
      else
        new_rating = rating + rate.to_i
      end
      article_rating.update_attribute(:rating, rate)
      update_attribute(:rating, new_rating)
    else
      ArticleRating.create(user_id: user_id, article_id: id, rating: rate)
      self.update_attribute(:rating, rating + rate.to_i)
    end
  end

  def self.from_subscribing_users_by(user)
    subscribing_users_ids = 'SELECT author_id FROM subscriptions
                         WHERE subscriber_id = :user_id'
    where("user_id IN (#{subscribing_users_ids}) OR user_id = :user_id",
          user_id: user.id)
  end

  def self.search(search)
    if search
      where('title LIKE :search OR text LIKE :search', search: "%#{search}%")
    else
      where(nil)
    end
  end
end
