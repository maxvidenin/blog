class Comment < ActiveRecord::Base
  belongs_to :article
  belongs_to :user
  has_many :comment_ratings, dependent: :destroy
  validates :body, presence: true, :length => {:maximum => 500}
  scope :recent, lambda { order('created_at DESC') }

  def user_rating(user)
    user_rating = comment_ratings.find_by(user_id: user.id)
    user_rating ? user_rating.rating : 0
  end

  def rate!(user_id, rate)
    comment_rating = CommentRating.find_by(user_id: user_id, comment_id: id)
    if comment_rating
      if comment_rating.rating != 0
        new_rating = (rating - comment_rating.rating) + rate.to_i
      else
        new_rating = rating + rate.to_i
      end
      comment_rating.update_attribute(:rating, rate)
      update_attribute(:rating, new_rating)
    else
      CommentRating.create(user_id: user_id, comment_id: id, rating: rate)
      self.update_attribute(:rating, rating + rate.to_i)
    end
  end
end
