class NewSubscriberMailWorker
  include Sidekiq::Worker

  def perform(author_id, subscriber_id)
    @author = User.find(author_id)
    @subscriber = User.find(subscriber_id)
    UserMailer.new_subscriber(@author, @subscriber).deliver
  end
end