class NewArticleMailWorker
  include Sidekiq::Worker

  def perform(article_id)
    article = Article.find(article_id)
    users = User.all
    users.each do |user|
      UserMailer.new_article(user, article).deliver
    end
  end
end