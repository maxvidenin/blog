namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_users
    make_articles
  end
end


def make_users
  20.times do |n|
    first_name = Faker::Name.first_name
    last_name = Faker::Name.last_name
    email = "#{first_name}-#{last_name}-#{n}@example.com"
    password = '123123123'
    User.create!(
        first_name: first_name,
        last_name: last_name,
        email: email,
        password: password
    )
  end
end

def make_articles
  users = User.all.limit(5)
  5.times do
    title = Faker::Lorem.sentence(5)
    text = Faker::Lorem.sentence(100)
    users.each { |user| user.articles.create!(title: title, text:text) }
  end
end