# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150405102420) do

  create_table "article_ratings", force: true do |t|
    t.integer  "user_id"
    t.integer  "article_id"
    t.integer  "rating",     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "article_ratings", ["article_id"], name: "index_article_ratings_on_article_id"
  add_index "article_ratings", ["user_id", "article_id"], name: "index_article_ratings_on_user_id_and_article_id", unique: true
  add_index "article_ratings", ["user_id"], name: "index_article_ratings_on_user_id"

  create_table "articles", force: true do |t|
    t.string   "title"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "rating",     default: 0
  end

  add_index "articles", ["user_id"], name: "index_articles_on_user_id"

  create_table "comment_ratings", force: true do |t|
    t.integer  "user_id"
    t.integer  "comment_id"
    t.integer  "rating",     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comment_ratings", ["comment_id"], name: "index_comment_ratings_on_comment_id"
  add_index "comment_ratings", ["user_id", "comment_id"], name: "index_comment_ratings_on_user_id_and_comment_id", unique: true
  add_index "comment_ratings", ["user_id"], name: "index_comment_ratings_on_user_id"

  create_table "comments", force: true do |t|
    t.text     "body"
    t.integer  "article_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "rating",     default: 0
  end

  add_index "comments", ["article_id"], name: "index_comments_on_article_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "subscriptions", force: true do |t|
    t.integer  "author_id"
    t.integer  "subscriber_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subscriptions", ["author_id", "subscriber_id"], name: "index_subscriptions_on_author_id_and_subscriber_id", unique: true
  add_index "subscriptions", ["author_id"], name: "index_subscriptions_on_author_id"
  add_index "subscriptions", ["subscriber_id"], name: "index_subscriptions_on_subscriber_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "role",                   default: "user"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
