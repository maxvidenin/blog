class CreateCommentRatings < ActiveRecord::Migration
  def change
    create_table :comment_ratings do |t|
      t.integer :user_id
      t.integer :comment_id
      t.integer :rating, default: 0

      t.timestamps
    end
    add_index :comment_ratings, :user_id
    add_index :comment_ratings, :comment_id
    add_index :comment_ratings, [:user_id, :comment_id], unique: true
  end
end
