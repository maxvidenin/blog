class CreateArticleRatings < ActiveRecord::Migration
  def change
    create_table :article_ratings do |t|
      t.integer :user_id
      t.integer :article_id
      t.integer :rating, default: 0

      t.timestamps
    end
    add_index :article_ratings, :user_id
    add_index :article_ratings, :article_id
    add_index :article_ratings, [:user_id, :article_id], unique: true
  end
end
